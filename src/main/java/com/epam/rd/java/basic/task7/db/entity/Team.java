package com.epam.rd.java.basic.task7.db.entity;

public class Team {
	private int id;
	private String name;

	public Team(String name) {
		this.name = name;
	}

	public Team(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public Team() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Team temp = (Team) obj;
		return (this.name.equals(temp.name));
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

}
