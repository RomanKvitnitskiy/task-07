package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;
	private String login;

	public User() {
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		User temp = (User) obj;
		return (this.login.equals(temp.login));
	}

	public static User createUser(String login) {
		return new User(login);
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public User(int id, String login) {
		this.id = id;
		this.login = login;
	}

	public User(String login) {
		this.login = login;
	}

	public String toString() {
		return login;
	}

}
